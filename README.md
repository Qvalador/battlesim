#BattleSIM
BattleSIM is a very simple, CLI game.  It's tested mainly in Ubuntu 16.04, though some light testing has been done in Windows and no significant problems have arisen.  There is currently no testing for Macs, but if you are a Mac-user and would like to do it for me, be my guest.  Note that the combat messages are mildly buggy; if such a bug arises for you, be sure to [submit a issue](https://bitbucket.org/Qvalador/battlesim/issues), and be sure to include exactly what went wrong and the context.

The premise of the game is streak fighting and eventually beating a boss.  If you die at any point, you're forced to start over, you can't run from battles, and you're only offered limited information about your opponent before accepting a fight.  It's intended to be quite hard.

##Getting Started
`git clone https://bitbucket.org/Qvalador/battlesim` in a good directory, and then `python3 game.py` to get it running.  The tutorial is not strictly necessary, but it might be good to play it the first time around for learning purposes.

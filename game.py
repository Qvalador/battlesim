### BattleSIM
### (C) Ryan Skylar, 2017

from util import *
import time
import sys
import random as rng

c = Colour()
        
rsay(c.italic("Once upon a time, in a land far, far away, created by the gods as a galactic battlefield..."))

rsay("It's a world they called...")

print(c.red("""______         _    _    _        _____  _____ ___  ___
| ___ \       | |  | |  | |      /  ___||_   _||  \/  |
| |_/ /  __ _ | |_ | |_ | |  ___ \ `--.   | |  | .  . |
| ___ \ / _` || __|| __|| | / _ \ `--. \  | |  | |\/| |
| |_/ /| (_| || |_ | |_ | ||  __//\__/ / _| |_ | |  | |
\____/  \__,_| \__| \__||_| \___|\____/  \___/ \_|  |_/ v1.2\n"""))


while True:
    tutorial = input("Would you like to play the tutorial? (y/n)\n> ")
    if tutorial == "y":
        rsay("You there.  What's your name?", "Captain")

        _name = c.bold(c.blue(input("> ")))

        rsay("Ah, {0}.  Okay.  Well, up and at 'em, {0}.".format(_name), c.bold("Captain"))
        rsay("I'm gonna teach you to fight.", "Captain")

        while True: # a True loop is probably not the best way to do this, but currently i'm using it to whisk you back if you have bad input.

            rsay("What kinda weapon is it you use?  Sword, hammer, magic?", "Captain")
            weaponchoice = input("> ").lower()
            
            weapondict = {"sword": [20, 20, .8, 0, "An all around weapon with a quick swing"], "hammer": [50, 30, .5, -1, "A crushing but unwieldy weapon"], "magic": [35, 10, .9, 1, "A specialised, precise weapon with mild power"]} # list of starter weapons and their values.

            if weaponchoice in weapondict:
                rsay("Ah, so you fight with {0}.  {1}.  Is that right?  (y/n)".format(c.green(weaponchoice), weapondict[weaponchoice][4]), c.bold("Captain"))
                answer = input("> ").lower() # get the player's weapon choice
                if answer == "y": # weapon creation
                    _weapon = Weapon(c.green(weaponchoice), weapondict[weaponchoice][0], weapondict[weaponchoice][1], weapondict[weaponchoice][2], weapondict[weaponchoice][3], weapondict[weaponchoice][4]) # surely there's some better way to do this.  this is atrocious.
                    break
                else:
                    print("\n") # if you don't say "y", you go back.  saying "pizza" and "no" don't have different effects, currently.
                    rsay("I see.", "Captain")
                    continue
            else:
                print("\n")  # make sure what they're choosing is an actual option.
                rsay("That's not a weapon.", "Captain")
                continue
                
        you = Player(_name, 29, "", _weapon) # it took us forever but here we finally are.  making you.
                
        rsay("Okay, great.  So {0} it is.".format(c.green(weaponchoice)), "Captain")
        rsay("Now's your chance to hit me.", "Captain")
        rsay("Go on.  {0} me.".format(c.cyan("Attack")), "Captain")

        capweapon = Weapon(c.green("Captain's Rifle"), 90, 0, .7, 2, "An advanced-looking rifle.  Best not to cross its path.") # we have to make weapons this way.
        Captain = Player(c.bold("Captain"), 300, "Your instructor.", capweapon) # and then create enemies using the player class.

        while True: # we have to simulate an actual battle, because having an actual one wouldn't allow for dialogue.  yet.
            action = input("> ").lower()
            if action == "attack":
                rsay("{0} attacks {1} with {2}!  {3} damage!".format(you.name, Captain.name, you.weapon.name, rng.randrange(1, 99)))
                break
            else: # the prompt isn't very clear, so he'll say this if you don't write 'attack' verbatim.  might clarify further later.
                rsay("Are you overthinking it?  Just {0} me.".format(c.cyan("attack"), "Captain"))
                continue
                
        rsay("Okay, good, yeah.  Just like that.", "Captain")
        rsay("Now it's my turn.  {0} my hit!".format(c.cyan("Block")), "Captain")

        while True:
            action = input("> ").lower()
            if action == "block":
                rsay("{0} attacks {1} with {2}!  {3} damage!".format(Captain.name, you.name, Captain.weapon.name, rng.randrange(100, 999)))
                rsay("{0} blocked {1} damage!".format(you.name, rng.randrange(1,99)))
                break
            else:
                rsay("Are you overthinking it?  Just {0} my hit.".format(c.cyan("block"), "Captain"))
                continue
                
        rsay("Yeah, you're getting the hang of it!", "Captain")
        rsay("So, {} inflicts damage, but leaves you wide open.".format(c.cyan("attacking")), "Captain")   # some review.
        rsay("{} doesn't inflict any damage, but it lets you heal.".format(c.cyan("Blocking")), "Captain") # maybe incorporate this stuff into the
        rsay("Okay! Let's get you your first opponent.", "Captain")                                        # tutorial fight instead of explaining it

        bone_club = Weapon(c.green("bone club"), 20, 10, .7, 0, "A crudely-fashioned club made from whittled bone and twine.") # make a dumb weapon
        enemy = Player(c.red("Generic Foe"), 20, "An average-looking dude.", bone_club) # make a dumb opponent

        combat(you, enemy) # fight it.  line 54+ handles all this.

        # hoooooooly story okay.  time for lots of talking and not much else.

        rsay("Nice, you beat him!  You're off to a good start!", "Captain")

        rsay(c.cyan("Occasionally, you will be prompted to speak. You'll be given several options, appropriately enumerated.  Choose your dialogue choice by typing the number, and nothing else, and hitting ENTER."))

        talk_choices = {"1": "...Did i just kill someone?", "2": "Thanks.  What is this place?"} # create the dict for talk()
        _talk = talk(you, talk_choices) # create a variable so we can do response-specific stuff
        if _talk == "1": # only say this if the user picks 1.  EXCLUSIVE CONTENT.  EXCITING.
            rsay("Yeah, i suppose you did.", "Captain")
            rsay("But don't worry about it.", "Captain")
            rsay("Everyone who comes here is aware of that risk.", "Captain")
            talk_choices = {"1": "What is this place?"}
            talk(you, talk_choices)
        rsay("What?  You don't know where you are?", "Captain")
        rsay("...", "Captain")
        rsay("Mate, you're in {0}.".format(c.red("BattleSIM")), "Captain")
        rsay("Roughest planet in this arm of the Milky Way.", "Captain")
        rsay("How on earth did you end up here on accident?", "Captain")
        talk_choices = {"1": "There must be some kind of mistake.", "2": "Oh, there was no accident."}
        _talk = talk(you, talk_choices)
        if _talk == 1:
            rsay("That's not good, but either way...", "Captain")
        if _talk == 2:
            rsay("Oh, hm.  You're a confusing one.", "Captain")
        rsay("Well, you're stuck here no matter what.  So.", "Captain")
        rsay("Maybe i could do to explain a bit.", "Captain")
        rsay("{0} is an old, old galactic tradition.  No one knows how it got here.".format(c.red("BattleSIM")), "Captain")
        rsay("But since the dawn of time, it's been a place for fighting.", "Captain")
        rsay("That's right.  Here on {0}, there are {1}.".format(c.red("BattleSIM"), c.red("no laws")), "Captain")
        rsay("It's every man for himself.", "Captain")
        if _talk == "1":
            rsay("Unfortunately, i'm not sure how long you'll make it...", "Captain")
        if _talk == "2":
            rsay("You seem the prepared type.  Perhaps you'll make it here.", "Captain")
        rsay("Only the strong survive.  Fortunately, there are ways of becoming strong.", "Captain")
        rsay("{0}.  It's the only way to push yourself forward in this world.".format(c.red("Combat")), "Captain")
        talk_choices = {"1": "Can't i leave?", "2": "..."}
        _talk = talk(you, talk_choices)
        if _talk == "1":
            rsay("If you worked up enough money to afford a ship and crew?  Sure.", "Captain")
            rsay("But you'd sooner become {0}.  I wouldn't count on it.".format(c.red("Pit King")), "Captain")
        rsay("The strongest fighter in this world is called the {0}.".format(c.red("Pit King")), "Captain")
        rsay("Anyone can challenge him, i suppose...", "Captain")
        rsay("But doing so without intense training would be suicide.", "Captain")

        talk(you, {"1": "So what do i do...?"})
        rsay("Tough question.", "Captain")
        rsay("Well, i suppose the only thing there is for you to do is {0}.".format(c.red("fight")), "Captain")
        rsay("{0} your way to the top.".format(c.red("Fight")), "Captain")
        rsay("You can do that by {0}ing to the {1}.".format(c.cyan("go"), c.red("arena")), "Captain")
        rsay("Going there will pair you with a random opponent.", "Captain")
        rsay("Once you're ready, you can {0}.".format(c.cyan("fight pit king")), "Captain")
        rsay("I wish you luck, my friend.", "Captain")
        break
    elif tutorial == "n":
        rsay("Enter your name.")
        _name = c.bold(c.blue(input("> ")))
        while True: # a True loop is probably not the best way to do this, but currently i'm using it to whisk you back if you have bad input.

            rsay("Choose a weapon.  (sword/hammer/magic)")
            weaponchoice = input("> ").lower()
            
            weapondict = {"sword": [20, 20, .8, 0, "An all around weapon with a quick swing"], "hammer": [50, 30, .5, -1, "A crushing but unwieldy weapon"], "magic": [35, 10, .9, 1, "A specialised, precise weapon with mild power"]} # list of starter weapons and their values.

            if weaponchoice in weapondict:
                rsay("Your choice is {0}.  {1}.  Is that right?  (y/n)".format(c.green(weaponchoice), weapondict[weaponchoice][4]))
                answer = input("> ").lower() # get the player's weapon choice
                if answer == "y": # weapon creation
                    _weapon = Weapon(c.green(weaponchoice), weapondict[weaponchoice][0], weapondict[weaponchoice][1], weapondict[weaponchoice][2], weapondict[weaponchoice][3], weapondict[weaponchoice][4]) # surely there's some better way to do this.  this is atrocious.
                    break
                else:
                    print("\n") # if you don't say "y", you go back.  saying "pizza" and "no" don't have different effects, currently.
                    continue
            else:
                print("\n")  # make sure what they're choosing is an actual option.
                rsay("That's not a weapon.")
                continue
        you = Player(_name, 30, "", _weapon)
        you.xp = 20
        break
        
while True:
    action = input("Try {0}, {1}, or {2}.\n> ".format(c.cyan("go arena"), c.cyan("fight pit king"), c.cyan("upgrade weapon"))).lower()
    if action == "go arena":
        enemy = get_enemy(you)
    elif action == "fight pit king":
        pitbane = Weapon("Pitbane", 40, 40, .9, 0, "A great, hulking sword that exudes power.")
        pit_king= Player(c.bold(c.red("Pit King")), 100, "The almighty Pit King, in the flesh!", pitbane)
        rsay("The {0} accepts your challenge, and approaches mightily...".format(c.bold(c.red("Pit King"))))
        rsay("It's a human, like you, but scarred beyond all recognition.")
        rsay("Keheheh!  So you wish to take my throne?  So be it!", c.bold(c.red("Pit King")))
        combat(you, pit_king)
    elif action == "upgrade weapon":
        if you.xp < 30:
            rsay("You need 30 xp for this.")
            continue
        if you.status == "repaired":
            rsay("You've already repaired your weapon.  Go fight something and try again.")
        else:
            answer = input("This will cost you 30 xp.  You're sure?(y/n)\n> ")
            if answer == "y":
                you.status = "repaired"
                you.weapon.power += 5
                you.weapon.defence += 5
                you.xp -= 30
                roll = rng.randrange(0, 3)
                if roll == 0 and you.weapon.accuracy < 1:
                    you.weapon.accuracy += .05
                    rsay("{0} power increased by 5 (total: {1}).  {0} defence increased by 5 (total: {2}).  {0} accuracy increased by 10% (total: {3}.".format(you.weapon.name, you.weapon.power, you.weapon.defence, ( you.weapon.accuracy * 100)))
                else:
                    rsay("{0} power increased by 5 (total: {1}).  {0} defence increased by 5 (total: {2}).".format(you.weapon.name, you.weapon.power, you.weapon.defence))
            else:
                continue
    elif action == "quit" or action == "q":
        sys.exit()                
    else:
        continue

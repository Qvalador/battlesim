import random as rng
import sys
import platform
import time

class Colour:

    def __init__(self):
        plat = platform.system()
        if plat == 'Linux':
            self._purple = '\033[95m'
            self._cyan = '\033[96m'
            self._darkcyan = '\033[36m'
            self._blue = '\033[94m'
            self._green = '\033[92m'
            self._yellow = '\033[93m'
            self._red = '\033[91m'
            self._bold = '\033[1m'
            self._underline = '\033[4m'
            self._italic = '\033[3m'
            self.end = '\033[0m'
        else:
            self._purple = ''
            self._cyan = ''
            self._darkcyan = ''
            self._blue = ''
            self._green = ''
            self._yellow = ''
            self._red = ''
            self._bold = ''
            self._underline = ''
            self._italic = ''
            self.end = ''
        
    def purple(self, message):
        """purplifies the text."""
        return ( self._purple + message + self.end )
        
    def cyan(self, message):
        """cyanifies the text."""
        return ( self._cyan + message + self.end )
        
    def darkcyan(self, message):
        """cyanifies the text, a la dark."""
        return ( self._darkcyan + message + self.end )
        
    def blue(self, message):
        """bluifies the text."""
        return ( self._blue + message + self.end )
        
    def green(self, message):
        """greenifies the text."""
        return ( self._green + message + self.end )
  
    def yellow(self, message):
        """yellofies the text."""
        return ( self._yellow + message + self.end )
        
    def red(self, message):
        """reddifies the text."""
        return ( self._red + message + self.end )
        
    def bold(self, message):
        """bolds the text."""
        return ( self._bold + message + self.end )
        
    def underline(self, message):
        """underlines the text."""
        return ( self._underline + message + self.end )
        
    def italic(self, message):
        """italicises the text."""
        return ( self._italic + message + self.end )
        
class Weapon:
    
    def __init__(self, name, power, defence, accuracy, priority, description):
        self.name = name
        self.power = power
        self.defence = defence
        self.accuracy = accuracy
        self.priority = priority
        self.description = description  
        self.turn = [0, 0, 0]  
        
    def __repr__(self):
        return ( "{0.name}. Pwr: {0.power}, Def: {0.defence}, Acc: {0.accuracy}, Pry: {0.priority}, Dsc: '{0.description}'".format(self))          
        
class Player:
    """the class representing the player character... or an enemy."""
    
    def __init__(self, name, hp, description, weapon):
      # YOU
        self.name = name
        self.maxhp = hp
        self.hp = self.maxhp
        self.description = description
        self.xp = 1
        self.kills = 1
        self.turn = [0, 0, 0] #first value is how you change your opponent's health; the second is how you change your own; the third is how you change the opponent's damage
        self.action = ""
      # WEAPON
        self.weapon = weapon
        self.stamina = 5 + self.weapon.priority        
        
    def __repr__(self):
        return ( "{3}: HP: {0}/{4}, ATK: {1}, DEF: {2}, STA: {5}".format(self.hp, self.weapon.power, self.weapon.defence, self.name, self.maxhp, self.stamina) )
        
    def attack(self):
        """attacks the enemy."""
        hit = rng.uniform(0, 1)
        if 0 < hit < self.weapon.accuracy:
            dmg = int((self.weapon.power * rng.uniform(.8, 1.2)) * (1 + (self.xp * .01))) # randomise damage slightly and account for xp.
            self.turn = [dmg, 0, 0]
            self.action = "attack"
            self.stamina -= 1
            return self.turn
        else:
            self.turn = [0, 0, 0, "{} missed!".format(self.name)]
            self.action = "miss"
            
    def block(self):
        """defends against an attack."""
        hit = rng.uniform(0, 1)
        if 0 < hit < self.weapon.accuracy:
            heal = rng.randrange(1, 10) # healing 0 hp is dumb.  but more than 10 is ridiculous.
            self.turn = [0, heal, self.weapon.defence]
            self.action = "block"
        else:
            self.turn = [0, 0, 0, "{} missed!".format(self.name)]
            self.action = "miss"
            
c = Colour()


def rsay(message, *speaker):
    """rpg-formatted print."""
    if speaker:
        talkname = speaker[0] + ":"
        msg = str(message)
        response = "{0} {1}".format(c.bold(talkname), msg) # input: rsay("stuff", "guy") gives you "guy: stuff"
        for letter in response:         # http://stackoverflow.com/a/9246096
            sys.stdout.write(letter)    # prints out letters one at a time.
            sys.stdout.flush()          # gives you a cool rpg aesthetic.
            time.sleep(0.02)            # also makes it possible to ingest large blocks of text.
#        print("{}".format(response))   # don't use this line and the above block simultaneously.  debug/test purposes only.
    elif not speaker:
        msg = str(message)
        for letter in msg:          # this is just in case we just want the game to
            sys.stdout.write(letter)    # say stuff, which happens sometimes.
            sys.stdout.flush()
            time.sleep(0.02)
#        print(message)
    print("\r")
    
def talk(you, choices):
    for l in choices:                            # requires "choices" to be a dict following format {"index": "text"}
        rsay(c.purple(choices[l]), c.purple(l))  # print all the options...
    while True:
        choice = str(input("> "))                # allow the player to pick one by its index...
        if choice in choices:
            rsay(choices[choice], you.name)      # say it aloud so it appears you're talking...
            return l                             # and then return the index so we can do response-specific stuff.
            break
        else:
            continue                             # if your input is bad, do nothing.

def get_combat_blurb(you, enemy):
    """let's make sure the player actually knows what's going on."""
    if you.action == "miss":                    # i guess missing is an action.
        return "{0} missed!".format(you.name)
    if you.action == "attack":
        return "{0} attacks {1} with {2}!  {3} damage!".format(you.name, enemy.name, you.weapon.name, you.turn[0]) # maybe add crit stuff.
    if you.action == "block":
        if enemy.action == "attack":
            return "{0} blocked {1} damage!".format(you.name, you.turn[2]) # you can only block if you're being hit.
        else:
            return "{0} healed for {1} damage!".format(you.name, you.turn[1]) # you heal if you aren't being hit, so make sure you say so.
            
def get_enemy(you):
    while True:
        adj_dict = ["Dark", "Bone", "Hell", "Shadow", "Blood", "Stench", "Evil", "Dastardly"]
        name_dict = ["Goblin", "Spirit", "Troll", "Thief", "Skeleton", "Dragon"]
        weapon_dict = ["Blade", "Sword", "Bow", "Whip", "Mace", "Rifle", "Hammer", "Staff", "Magic"]
        priorities = [0, 0, 0, 0, 0, 0, -1, -1, -1, 1, 1]
        a = rng.choice(adj_dict)
        n =  rng.choice(name_dict)
        wa = rng.choice(adj_dict)
        wn = rng.choice(weapon_dict)
        name = c.red(" ".join([a, n]))
        hp = ( int(you.hp * rng.uniform(1, 1.7)) )
        description = "A {0} of the {1} variety.".format(a, n)
        wname = c.green(" ".join([wa, wn]))
        wpower = ( int(you.weapon.power * rng.uniform(.6, 1.2)) )
        wdef = ( int(you.weapon.defence * rng.uniform(.6, 1.2) ) )
        pri = rng.choice(priorities)
        acc = round(rng.uniform(.5, 1), 1)
        wdsc = "A {0} of the {1} variety.".format(wn, wa)
        
        weapon = Weapon(wname, wpower, wdef, acc, pri, wdsc)
        enemy = Player(name, hp, description, weapon)
        answer = input("Would you like to fight {0} [{1}/{1}]? (y/n)\n> ".format(enemy.name, enemy.hp))
        if answer == "y":
            combat(you, enemy)
            break
        else:
            break

def combat(you, enemy):
    """fight!  fight!  fight!"""
    def check_win(you, enemy):
        if you.hp < 1 or you.stamina < 0:
            rsay("Oh dear, you've lost!")
            sys.exit()
        if enemy.hp < 1 or enemy.stamina < 0:
            you.xp += enemy.maxhp  # and grants you xp equal to the opponent's hp,
            you.kills += 1         # while also increasing your kill count by one.
            you.maxhp += 1
            you.hp = you.maxhp
            rsay("Won! Got {0} xp (total: {1})!  Hp increased by 1 (total: {2})!".format(enemy.maxhp, you.xp, you.hp), you.name)
            you.status = None
            you.turn = [0, 0, 0]    # reset the turn lists
            enemy.turn = [0, 0, 0]  # for a fresh start next turn.            
            return True
        else:
            return False
            
    you.status = "combat"
    rsay("{0} appears!  What will {1} do? [attack/block]".format(enemy.name, you.name))
    while True:
        you_action_dict = {"attack": you.attack, "block": you.block} # acceptable inputs and their respective actions
        print(enemy) # give a summary of the enemy
        print(you)   # and yourself at the beginning of each turn
        
        you_action = input("> ")                    # allow action input,
        if you_action in you_action_dict:           # check if that action is allowed,
            you_turn = you_action_dict[you_action]  # and then set your turn to be that action...
        else:                                       # or else don't allow that and try again.
            rsay("Invalid action.")
            continue
            
        enemy_action = rng.choice([enemy.attack, enemy.block]) # enemy actions are random.  maybe add "ai" later.
      
        enemy_action()
        you_turn()            
        
        damage_dealt_you = you.turn[0] - enemy.turn[2]    # calculate damage dealt by subtracting the target's defence from
        damage_dealt_enemy = enemy.turn[0] - you.turn[2]  # the aggressor's attack.
        damage_healed_enemy = enemy.turn[1] # just so i don't have to go and re-check what enemy.turn[1] is a million times.
        damage_healed_you = you.turn[1]
        
        if enemy.stamina > you.stamina:
        
            if damage_dealt_enemy > 0: # we don't want you restoring a ton of health if your block is more than the foe's attack damage.
                you.hp -= damage_dealt_enemy
                rsay(get_combat_blurb(enemy, you))
                if check_win(you, enemy):
                    break
                
            if damage_dealt_you > 0:
                enemy.hp -= damage_dealt_you
                rsay(get_combat_blurb(you, enemy)) 
                if check_win(you, enemy):
                    break          
                    
            if enemy.action != "attack" and you.action != "miss":
                rsay(get_combat_blurb(enemy, you))
            if you.action != "attack" and enemy.action != "miss":
                rsay(get_combat_blurb(you, enemy))                          
                
        else:
        
            if damage_dealt_you > 0:
                enemy.hp -= damage_dealt_you  
                rsay(get_combat_blurb(you, enemy)) 
                if check_win(you, enemy):
                    break                    
                
            if damage_dealt_enemy > 0: # we don't want you restoring a ton of health if your block is more than the foe's attack damage.
                you.hp -= damage_dealt_enemy
                rsay(get_combat_blurb(enemy, you)) 
                if check_win(you, enemy):
                    break    
                               
            if you.action != "attack" and enemy.action != "miss":
                rsay(get_combat_blurb(you, enemy))                      
            if enemy.action != "attack" and you.action != "miss":
                rsay(get_combat_blurb(enemy, you))
            
        if you.action == "miss" and you.action == "block": # only heal if the opponent isn't attacking.
            if enemy.hp < enemy.maxhp:
                enemy.hp += damage_healed_enemy
                if enemy.hp > enemy.maxhp:
                    enemy.hp = enemy.maxhp
                rsay(get_combat_blurb(enemy, you))
            
        if enemy.action == "miss" and enemy.action == "block": # only heal if the opponent isn't attacking.
            if you.hp < you.maxhp:
                you.hp += damage_healed_you
                if you.hp > you.maxhp:
                    you.hp = you.maxhp
                rsay(get_combat_blurb(you, enemy))
